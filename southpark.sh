#/usr/bin/bash

set -e

CONFIG_DIR=~/.config/southpark # Change to your liking
NUM_OF_SEASONS=20 # Modify as new seasons appear
EPISODES_PER_SEASON=(13 18 17 17 14 17 15 15 14 14 14 14 14 14 14 14 10 10 10 10)

function display_usage {
	echo "southpark.sh <option> <value>..."
	echo "Options:"
	echo "\t-s, --season <season number>"
	echo "\t-e, --episode <episode number>"
	echo "\t-u, --url <url> (only justanimedubbed supported)"
	echo "\t-n, --next, --sequential. Changes the episode order to sequential."
	echo "\t-r, --random. Plays a random episode, changes the order to random."
	echo "\t-i, --ignore. Don't count this episode as seen."
}

while [ $# -gt 0 ]
do
	key="$1"

	case $key in
		-u|--url)
		URL="$2"
		shift
		;;
		-s|--season)
		season="$2"
		shift
		;;
		-e|--episode)
		episode="$2"
		shift
		;;
		-h|--help)
		display_usage
		;;
		-n|--next|--sequential)
		sequential_mode="true"
		;;
		-r|--random)
		random_mode="true"
		;;
		-i|--ignore)
		ignore_view="true"
		;;
	esac
	shift
done

if [ ! -d $CONFIG_DIR ]; then
	mkdir -p $CONFIG_DIR;
fi

function set_play_mode { # Sequential or random order
	if [ -n "$sequential_mode" -a -n "$random_mode" ]; then
		sequential_mode=""
	fi

	if [ -n "$sequential_mode" ]; then
		touch "$CONFIG_DIR/sequential"
	elif [ -n "$random_mode" ]; then
		rm -f "$CONFIG_DIR/sequential"
	fi
}

function read_play_mode {
	if [ -e "$CONFIG_DIR/sequential" ]; then
		mode="sequential"
	else
		mode="random"
	fi
}

function episode_seen {
	if [ -z "$season" -o -z "$episode" ]; then
		return 0;
	fi

	if [ -e "$CONFIG_DIR/$season-$episode" ]; then
		return 0;
	fi

	return -1;
}

function play_episode { # Requires only $season and $episode
	url="http://www.justanimedubbed.tv/south-park-season-$season-episode-$episode/"
	final_url=$(curl -s $url | grep -o '"http://uploadcrazy[^"]*"' | tr -d '"') # Can be slow

	if [ -z $ignore_view ]; then
		touch "$CONFIG_DIR/$season-$episode"
	else
		echo "Not registering view"
	fi

	echo "Opening $url"
	gnome-mpv "$final_url"
}

function play_random {
	times_called=0

	while episode_seen && [ $times_called -le 100 ]; do
		season=$(($RANDOM % $NUM_OF_SEASONS + 1))
		episode=$(($RANDOM % ${EPISODES_PER_SEASON[$(($season - 1))]} + 1))
		times_called=$(($times_called + 1))
	done

	echo "Tried $times_called times, playing $season-$episode"

	play_episode
}

function play_sequential {
	if [ ! -e "$CONFIG_DIR/current" ]; then
		echo "1-1" > "$CONFIG_DIR/current"
	fi

	content=$(<"$CONFIG_DIR/current")

	if [[ $content =~ ([0-9]+)-([0-9]+) ]]; then
		season=${BASH_REMATCH[1]}
		episode=${BASH_REMATCH[2]}
	else
		rm "$CONFIG_DIR/current" # Garbage content, throw it out
		echo "Unable to read current episode, restart the script."
		exit 1
	fi

	if [ "$episode" == "${EPISODES_PER_SEASON[$(($season - 1))]}" ]; then
		if [ "$season" -ge $NUM_OF_SEASONS ]; then
			echo "You've watched all of South Park"
			exit 0
		fi
		nseason=$(($season + 1))
		nepisode="1"
	else
		nseason=$season
		nepisode=$(($episode + 1))
	fi

	echo "$nseason-$nepisode" > "$CONFIG_DIR/current"

	play_episode
}

if [ -n "$sequential_mode" -o -n "$random_mode" ]; then
	set_play_mode
fi

read_play_mode

if [ -n "$season" -a -n "$episode" ]; then
	play_episode
elif [ $mode == "sequential" ]; then
	play_sequential
else
	play_random
fi
